# Zadanie 1
## 1.A. W jakim celu używa się klas abstrakcyjnych, a w jakim interfejsów?
Klasy abstrakcyjne pozwalają zaimplementować pewną część funkcji, a pozostawić resztę z nich jako funkcje abstrakcyjne, czyli tylko w postaci deklaracji. Przy dziedziczeniu z tych klas trzeba zaimplementować te funkcje. Możemy dzięki temu stworzyć klasę bazową, która zawiera kod wspólny dla wszystkich dziedziczących, a pozostawić tylko część potrzebną do zaimplementowania odpowiedniej funkcjonalności. Przykładem może być tworzenie efektów przetwarzających sygnał dźwiękowy (takich jak przester lub reverb). W klasie abstrakcyjnej zaimplementowane byłyby wszystkie funkcje związane z czytaniem wejścia, zapisem do wyjścia, obsługą buforów itp. poza funkcją abstrakcyjną `process()`. Przy tworzeniu klas realizujących konkretny efekt musielibyśmy zaimplementować tylko jedną funkcję przetwarzającą próbki dźwiękowe i unikniemy powtarzania kodu.

Interfejsy pozwalają na określenie zestawu metod, które klasa musi posiadać, ale tylko w postaci deklaracji. Każda klasa implementująca interfejs musi zdefiniować te metody. Przy tworzeniu klas daje nam to sprawdzanie błędów. Przy korzystaniu z obiektów (np. interfejs jako parametr funkcji) powoduje to, że działamy w strefie abstrakcji i nie interesuje nas jakiego typu jest obiekt, a tylko jego funkcjonalność. Na przykład: interfejs `Writer` mógłby mieć opisany zestaw metod służących do zapisywania danych. Klasa która przetwarza i zapisuje dane wykorzystywałaby do tego obiekt realizujący ten interfejs. W zależności od wymagań możliwe jest stworzenie wielu różnych klas implementujących `Writer` do zapisywania do pliku, konsoli, bazy danych lub wysyłania przez sieć bez ingerencji w obiekt przetwarzający dane.

W Javie klasy mogą dziedziczyć tylko z jednej klasy, ale mogą implementować wiele interfejsów.

## 1.B. Czym różni się tablica od listy liniowej?
Tablica jest strukturą danych, która charakteryzuje się tym, że wszystkie jej elementy znajdują się jeden po drugim w pamięci komputera. Pozwala to na szybki dostęp do danych na podstawie indeksu, ponieważ wiążę się to tylko z obliczeniem $adres\\_elementu = rozmiar\\_elementu * indeks\\_elementu + adres\\_początkowy$. Wadami tablic jest to, że nie można dynamicznie zmienić ich rozmiaru oraz to że operacje wstawiania i usuwania danych w środek lub na początek wiążą się z kopiowaniem innych elementów.

Elementy listy liniowej, w przeciwieństwie do tablicy, mogą znajdować się w niezależnych od siebie miejscach pamięci. Każdy element listy nie tylko przechowuje dane, ale także wskaźnik na następny (i poprzedni dla listy dwu-kierunkowej) element. Taka struktura powoduje, że operacje wstawiania i usuwania elementów jest szybka (trzeba tylko zmienić wskaźniki) bez względu na miejsce elementu. Rozmiar listy może się dynamicznie zmieniać w przeciwieństwie do tablicy. Wadą jest wolniejszy dostęp do danych, ponieważ listę trzeba przejść po każdym elemencie od początku do żądanego.

# Zadanie 2
```java
private static boolean search(int[] numbers, int x) {
	int begin = 0;
	int end = numbers.length - 1;
	int middle;
	while (begin != end) {
		middle = (begin + end) / 2;
		if (numbers[middle] <= x) {
			end = middle;
		} else {
			begin = middle + 1;
		}
	}
	return numbers[begin] == x;
}
```

Złożoność obliczeniowa: $O(log\ n)$ - z każdym przejściem pętli, zmniejszamy ilość danych do zbadania o połowę.

Złożoność pamięciowa: $O(1)$ - wykorzystywana jest stała liczba zmiennych niezależna od ilości danych.

[Pełne rozwiązanie w /Zadanie2](/Zadanie2)

# Zadanie 3
[Rozwiązanie w /Zadanie3](/Zadanie3)
