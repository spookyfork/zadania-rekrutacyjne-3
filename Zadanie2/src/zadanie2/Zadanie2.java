package zadanie2;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/**
 * Mając tablicę int[] numbers, wypełnioną liczbami całkowitymi i posortowaną
 * malejąco ( numbers[i] > numbers[i+1] ), sprawdź czy występuje w niej liczba
 * int x. Metoda powinna zwracać wartość TRUE, jeśli dana liczba występuje oraz
 * FALSE w przeciwnym wypadku. W rozwiązaniu zależy nam na jak najmniejszej
 * złożoności obliczeniowej (priorytet) oraz pamięciowej. Podaj szacowaną
 * złożoność obliczeniową oraz pamięciową. Poniżej sygnatura metody do
 * napisania.
 *
 * private boolean search(int[] numbers, int x){}
 *
 * @author Sebastian Widemajer
 */
public class Zadanie2 {

    private static int[] generateSortedArray(int size) {
        Random rand = new Random();
        int origin = 0;
        int bound = 100;
        int[] array = new int[size];
        for (int i = size - 1; i >= 0; i--) {
            array[i] = rand.nextInt(origin, bound);
            origin = array[i] + 1;
            if (array[i] == bound - 1) {
                bound += 100;
            }
        }
        return array;
    }

    private static boolean search(int[] numbers, int x) {
        int begin = 0;
        int end = numbers.length - 1;
        int middle;
        while (begin != end) {
            middle = (begin + end) / 2;
            if (numbers[middle] <= x) {
                end = middle;
            } else {
                begin = middle + 1;
            }
        }
        return numbers[begin] == x;
    }

    public static void main(String[] args) {
        Random rand = new Random();
        int[] array = generateSortedArray(rand.nextInt(10, 20));
        System.out.println("Array:\n" + Arrays.toString(array));
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter integer to find: ");
            String s = br.readLine();
            int x = Integer.parseInt(s);
            System.out.println(search(array, x));
        } catch (IOError | IOException | NullPointerException | NumberFormatException e) {
            System.out.println("An unexpected error occurred: " + e);
            System.exit(1);
        }
    }
}
