package zadanie3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Napisz program, który wykorzysta API Kanye Rest https://kanye.rest/ by
 * każdorazowo zaproponować nową perełkę mądrości od Kanye Westa. Program
 * powinien być obsługiwany z poziomu konsoli i obsługiwać komendę "next" by
 * wywołać następny cytat. Program nie potrzebuje oprawy graficznej. Zwróć uwagę
 * na poprawną architekturę aplikacji oraz na czystość kodu. Dla chętnych, za
 * dodatkowe punkty: dodaj zapisywanie cytatów w pamięci, by upewnić się, że
 * każdy kolejny cytat jest nowy.
 *
 * @author Sebastian Widemajer
 */
public class Zadanie3 {

    public static void main(String[] args) {
        KanyeAPI api = new KanyeAPI();
        boolean shouldQuit = false;
        try {
            System.out.println(api.getWisdom());
            while (!shouldQuit) {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Type (n)ext or (q)uit: ");
                String s = br.readLine();
                s = s.strip().toLowerCase();
                switch (s) {
                    case "q", "quit" ->
                        shouldQuit = true;
                    case "n", "next" ->
                        System.out.println(api.getWisdom());
                    default ->
                        System.out.println("Command not recognized. Please try again.");
                }
            }
        } catch (IOException ex) {
            System.out.println("An unexpected error occurred: " + ex);
            System.exit(1);
        }
    }

}
