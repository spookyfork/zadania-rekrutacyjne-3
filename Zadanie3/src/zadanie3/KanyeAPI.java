package zadanie3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Connects to kanye.rest api to requests one of his quotes.
 * @author Sebastian Widemajer
 */
public class KanyeAPI {

    private static final String ENDPOINT = "https://api.kanye.rest/";

    /**
     * Maximum number of requests sent to the api in an attempt at getting 
     * a previously not received Kanye quote.
     */
    public static int MAX_REPEATS = 5;

    private TreeSet<String> memory;
    private boolean avoidRepeats;

    public KanyeAPI() {
        this.avoidRepeats = true;
        memory = new TreeSet<>();
    }

    public KanyeAPI(boolean avoidRepeats) {
        this.avoidRepeats = avoidRepeats;
        memory = new TreeSet<>();
    }

    public boolean isAvoidRepeats() {
        return avoidRepeats;
    }

    /**
     * Set if we should avoid repeating wisdoms
     *
     * @param avoidRepeats
     */
    public void setAvoidRepeats(boolean avoidRepeats) {
        this.avoidRepeats = avoidRepeats;
    }

    /**
     * Connects to kanye.rest API to get one of his quotes. If
     * {@code avoidRepeats} is {@code true} and received quote has already
     * previously been obtained, the operation will repeat. This will continue
     * until a new quote is received or until number of tries reaches
     * {@code MAX_REPEATS}.
     *
     * @return Kanye's wisdom.
     * @throws IOException if an I/O exception occurs.
     * @throws java.io.FileNotFoundException if there was an error (for instance
     * 404 error)
     * @throws java.net.UnknownHostException if the api endpoint address couldn't
     * be resolved
     */
    public String getWisdom() throws IOException {
        int tries = 0;
        String line = "";
        do {
            try {
                URL url = new URL(ENDPOINT);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                BufferedReader stream = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                line = stream.readLine();
                //This could be done with a JSON parser   
                //For instance, with https://github.com/stleary/JSON-java
                //We could use the following 2 lines:
                //org.json.JSONObject obj = new org.json.JSONObject(line);
                //String quote = obj.getString("quote");
                line = line.replaceAll("\\{|\\}|\t|\n", "").split(":")[1].strip();
                if (avoidRepeats) {
                    if (memory.contains(line)) {
                        continue;
                    } else {
                    }
                }
                //Always store received quotes, even if we don't avoid repeats.
                memory.add(line);
                return line;
            } catch (MalformedURLException ex) {
                //this should never happen
                Logger.getLogger(KanyeAPI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (avoidRepeats && ++tries < MAX_REPEATS);
        if (tries == MAX_REPEATS) {
            //We did not get anything new in MAX_REPEATS
            //We could throw an exception here.
        }
        return line;
    }
    
    /**
     * Returns an array containing all of the quotes received from Kanye.rest.
     *
     * @return an array containing all already received Kanye wisdoms.
     */
    public String[] getPreviousWisdoms() {
        return memory.toArray(String[]::new);
    }
}
